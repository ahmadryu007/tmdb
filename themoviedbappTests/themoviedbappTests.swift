//
//  themoviedbappTests.swift
//  themoviedbappTests
//
//  Created by BAGOES123 on 23/05/22.
//

import XCTest
@testable import themoviedbapp

class themoviedbappTests: XCTestCase {
    
    var viewController: MovieViewController?
    var detailViewController: MovieDetailViewController?
    
    
    override func setUp() {
        viewController = MovieRouter.createModule() as? MovieViewController
    }

    func testMovieViewController() throws {
        testMovie()
        testDetailMovie()
    }
    
    func testMovie() {
        XCTAssertTrue(viewController != nil)
        XCTAssertTrue(viewController?.presenter != nil)
        XCTAssertTrue(viewController?.presenter?.interactor != nil)
        
        viewController?.presenter?.loadMovies(page: 1, keyword: nil)
        let mockResponse = try? JSONDecoder().decode(MovieResponse.self, from: MockData.getMockDataResponse(type: .movieResponse)!)
        viewController?.presenter?.movies = mockResponse?.results ?? []
        viewController?.updateMoviesData()
        
        XCTAssertTrue((viewController?.presenter?.movies.count)! > 1)
        
        
        guard let tableView = viewController?.tableView else { return  }
        let indexPaths = tableView.indexPathsForVisibleRows
        
        for indexPath in indexPaths ?? [] {
            let cell = tableView.cellForRow(at: indexPath) as? MovieTableViewCell
            XCTAssertNil(cell?.movieTitleLabel.text)
            XCTAssertNil(cell?.movieYearLabel.text)
            XCTAssertNil(cell?.movieCategoryLabel.text)
        }
    }
    
    func testDetailMovie() {
        
    }
    

}
