//
//  MovieRouter.swift
//  themoviedbapp
//
//  Created by BAGOES123 on 23/05/22.
//

import Foundation
import UIKit

class MovieRouter: MoviePresenterToRouterProtocol {
    static func createModule() -> UIViewController {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let interactor = MovieInteractor()
        let presenter = MoviePresenter()
        let view = storyboard.instantiateViewController(withIdentifier: "MovieViewController") as! MovieViewController
        
        presenter.interactor = interactor
        interactor.presenter = presenter
        presenter.view = view
        view.presenter = presenter
        
        return view
        
    }
    
    static func goToDetail(movies: Movie) -> UIViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let interactor = MovieInteractor()
        let presenter = MoviePresenter()
        let view = storyboard.instantiateViewController(withIdentifier: "MovieDetailViewController") as! MovieDetailViewController
        
        presenter.interactor = interactor
        interactor.presenter = presenter
        presenter.view = view
        view.presenter = presenter
        view.movie = movies
        
        return view
    }
    
}
