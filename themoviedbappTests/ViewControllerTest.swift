//
//  ViewControllerTest.swift
//  themoviedbappTests
//
//  Created by BAGOES123 on 24/05/22.
//

import XCTest
@testable import themoviedbapp

class ViewControllerTest: XCTestCase {
    
    var rootWindow: UIWindow {
        return appDelegate.window!
    }
    
    override func setUp() {
        appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
        appDelegate.window?.makeKeyAndVisible()
        
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let navController: UINavigationController? = storyboard.instantiateInitialViewController() as? UINavigationController
        
        XCTAssertNotNil(navController)
        appDelegate.window?.rootViewController = navController
    }

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        appDelegate.window = nil
    }

    func testExample() throws {
        XCTAssertNil(rootWindow.rootViewController)
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}

extension XCTestCase {
    var appDelegate: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    func wait(timeout: TimeInterval) {
        let expectation = XCTestExpectation(description: "Waiting for \(timeout) seconds")
        XCTWaiter().wait(for: [expectation], timeout: timeout)
    }
}
