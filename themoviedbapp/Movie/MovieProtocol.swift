//
//  MovieProtocol.swift
//  themoviedbapp
//
//  Created by BAGOES123 on 23/05/22.
//

import Foundation
import UIKit


protocol MoviePresenterToViewProtocol: AnyObject {
    func updateMoviesData()
    func showError()
}

protocol MovieViewToPresenterProtocol: AnyObject {
    func loadMovies(page: Int, keyword: String?)
    func loadMovieCredits(id: Int)
}

protocol MovieInteractorToPresenterProtocol: AnyObject {
    func moviesLoaded(entity: MovieResponse)
    func movieCreditsLoaded(creditResponse: CreditsResponse)
    func moviesLoadFailed()
}

protocol MoviePresenterToInteractorProtocol: AnyObject {
    func getMovies(page: Int, keyword: String?)
    func getMovieCredits(id: Int)
}

protocol MoviePresenterToRouterProtocol: AnyObject {
    static func createModule() -> UIViewController
    static func goToDetail(movies: Movie) -> UIViewController
}
