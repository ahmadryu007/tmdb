//
//  CreditsCollectionViewCell.swift
//  themoviedbapp
//
//  Created by BAGOES123 on 24/05/22.
//

import UIKit

class CreditsCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var creditImageView: UIImageView!
    @IBOutlet weak var creditNameLabel: UILabel!
    
    func bindData(cast: Cast) {
        creditImageView.layer.cornerRadius = 25
        creditImageView.layer.masksToBounds = true
        
        creditNameLabel.text = cast.name
        if let imageUrl = URL(string: "\(Constant.imageBaseUrl)\(cast.profilePath ?? "")") {
            creditImageView.load(url: imageUrl)
        }
    }
    
}
