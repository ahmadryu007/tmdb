//
//  Constant.swift
//  themoviedbapp
//
//  Created by BAGOES123 on 23/05/22.
//

import Foundation

struct Constant {
    
    static let tmdbApiKey = "a3cd701cfc65f2e9863dc70fa8ea6d5e"
    static let genreDictionary = [
        28 : "Action",
        12 : "Adventure",
        16 : "Animation",
        35 : "Comedy",
        80 : "Crime",
        99 : "Documentary",
        18 : "Drama",
        10751 : "Family",
        14 : "Fantasy",
        36 : "History",
        27 : "Horror",
        10402 : "Music",
        9648 : "Mystery",
        10749 : "Romance",
        878 : "Science Fiction",
        10770 : "TV Movie",
        53 : "Thriller",
        10752 : "War",
        37 : "Western"
    ]
    
    static let baseUrl = "https://api.themoviedb.org/3/"
    static let imageBaseUrl = "https://image.tmdb.org/t/p/w200"
    static let getMoviesUrl = baseUrl + "discover/movie"
    static let searchMoviesUrl = baseUrl + "search/movie"
    static let creditsMoviesUrl = baseUrl + "movie/"
}
