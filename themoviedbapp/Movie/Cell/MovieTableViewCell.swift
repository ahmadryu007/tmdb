//
//  MovieTableViewCell.swift
//  themoviedbapp
//
//  Created by BAGOES123 on 23/05/22.
//

import UIKit

class MovieTableViewCell: UITableViewCell {
    
    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var movieTitleLabel: UILabel!
    @IBOutlet weak var movieYearLabel: UILabel!
    @IBOutlet weak var movieCategoryLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func bindData(movie: Movie) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let releaseDate = dateFormatter.date(from: movie.releaseDate ?? "")
        
        dateFormatter.dateFormat = "yyyy"
        let releaseYear = dateFormatter.string(from: releaseDate ?? Date())
        
        var categories: [String] = []
        for categoryId in movie.genreIDS ?? [] {
            categories.append(Constant.genreDictionary[categoryId] ?? "")
        }
        
        movieTitleLabel.text = movie.title
        movieYearLabel.text = releaseYear
        movieCategoryLabel.text = categories.joined(separator: ",")
        
        if let imageUrl = URL(string: "\(Constant.imageBaseUrl)\(movie.posterPath ?? "")") {
            movieImageView.load(url: imageUrl)
        }
    }

}
