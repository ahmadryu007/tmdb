//
//  MovieInteractor.swift
//  themoviedbapp
//
//  Created by BAGOES123 on 23/05/22.
//

import Foundation

class MovieInteractor: MoviePresenterToInteractorProtocol {
    
    weak var presenter: MovieInteractorToPresenterProtocol?
    
    func getMovies(page: Int, keyword: String?) {
        
        var urlString = "\(Constant.getMoviesUrl)?api_key=\(Constant.tmdbApiKey)&page=\(page)"
        if let keyword = keyword {
            urlString = "\(Constant.searchMoviesUrl)?api_key=\(Constant.tmdbApiKey)&page=\(page)&query=\(keyword.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? "")"
        }
        
        if let url = URL(string: urlString) {
            let urlSession = URLSession.shared
            let task = urlSession.dataTask(with: url) { data, urlResponse, err in
                if (err == nil ) {
                    if let jsonData = data {
                        let response = try? JSONDecoder().decode(MovieResponse.self, from: jsonData)
                        guard let movieResponse = response else { return }
                        self.presenter?.moviesLoaded(entity: movieResponse)
                    }
                } else {
                    self.presenter?.moviesLoadFailed()
                }
            }
            
            task.resume()
        }
    }
    
    func getMovieCredits(id: Int) {
        if let url = URL(string: "\(Constant.creditsMoviesUrl)\(id)/credits?api_key=\(Constant.tmdbApiKey)") {
            let urlSession = URLSession.shared
            let task = urlSession.dataTask(with: url) { data, urlResponse, err in
                if (err == nil ) {
                    if let jsonData = data {
                        let response = try? JSONDecoder().decode(CreditsResponse.self, from: jsonData)
                        guard let movieCreditsResponse = response else { return }
                        self.presenter?.movieCreditsLoaded(creditResponse: movieCreditsResponse)
                    }
                } else {
                    self.presenter?.moviesLoadFailed()
                }
            }
            
            task.resume()
        }
    }
}
