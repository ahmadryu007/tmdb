//
//  MoviePresenter.swift
//  themoviedbapp
//
//  Created by BAGOES123 on 23/05/22.
//

import Foundation

class MoviePresenter: MovieInteractorToPresenterProtocol {
    
    var interactor: MoviePresenterToInteractorProtocol?
    var view: MoviePresenterToViewProtocol?
    
    var movies: [Movie] = []
    var creditsMovie: CreditsResponse?
    
    func moviesLoaded(entity: MovieResponse) {
        movies.append(contentsOf: entity.results ?? [])
        view?.updateMoviesData()
    }
    
    func movieCreditsLoaded(creditResponse: CreditsResponse) {
        self.creditsMovie = creditResponse
        view?.updateMoviesData()
    }
    
    func moviesLoadFailed() {
        view?.showError()
    }
    
}

extension MoviePresenter: MovieViewToPresenterProtocol {
    func loadMovies(page: Int, keyword: String?) {
        interactor?.getMovies(page: page, keyword: keyword)
    }
    
    func loadMovieCredits(id: Int) {
        interactor?.getMovieCredits(id: id)
    }
}
