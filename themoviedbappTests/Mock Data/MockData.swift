//
//  MockData.swift
//  themoviedbappTests
//
//  Created by BAGOES123 on 24/05/22.
//

import Foundation

enum MockDataType: String {
    case movieResponse = "MockMovieResponse"
    case creditsResponse = "MockCreditResponse"
}

class MockData {
    
    static func getMockDataResponse(type: MockDataType) -> Data? {
        if let data = readLocalFile(forName: type.rawValue) {
            return data
        }
        return nil
    }
    
    private static func readLocalFile(forName name: String) -> Data? {
        do {
            if let bundlePath = Bundle(for: MockData.self).path(forResource: name, ofType: "json"),
               let jsonData = try String(contentsOfFile: bundlePath).data(using: .utf8) {
                return jsonData
            }
        } catch {
            print(error)
        }
        
        return nil
    }
}
