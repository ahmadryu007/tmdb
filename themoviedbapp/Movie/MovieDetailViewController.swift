//
//  MovieDetailViewController.swift
//  themoviedbapp
//
//  Created by BAGOES123 on 23/05/22.
//

import UIKit

class MovieDetailViewController: UIViewController {
    
    var presenter: MoviePresenter?
    var movie: Movie?

    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var movieTitleLabel: UILabel!
    @IBOutlet weak var movieTimeLabel: UILabel!
    @IBOutlet weak var movieCategoriesLabel: UILabel!
    @IBOutlet weak var movieDescription: UILabel!
    @IBOutlet weak var creditsCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
        flowLayout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        flowLayout.minimumInteritemSpacing = 0.0
        creditsCollectionView.collectionViewLayout = flowLayout
        
        creditsCollectionView.delegate = self
        creditsCollectionView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        presenter?.loadMovieCredits(id: movie?.id ?? 0)
        movieTitleLabel.text = movie?.title
        movieDescription.text = movie?.overview
        
        var categories: [String] = []
        for categoryId in movie?.genreIDS ?? [] {
            categories.append(Constant.genreDictionary[categoryId] ?? "")
        }
        movieCategoriesLabel.text = categories.joined(separator: ",")
        
        if let imageUrl = URL(string: "\(Constant.imageBaseUrl)\(movie?.backdropPath ?? "")") {
            movieImageView.load(url: imageUrl)
        }
        
    }

}

extension MovieDetailViewController: MoviePresenterToViewProtocol {
    
    func updateMoviesData() {
        DispatchQueue.main.async {
            self.creditsCollectionView.reloadData()
        }
    }
    
    func showError() {
        
    }
    
}

extension MovieDetailViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter?.creditsMovie?.cast?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CreditsCell", for: indexPath) as? CreditsCollectionViewCell
        
        if let cast = presenter?.creditsMovie?.cast?[indexPath.row] {
            cell?.bindData(cast: cast)
        }
        
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 100)
    }
    
    
}
