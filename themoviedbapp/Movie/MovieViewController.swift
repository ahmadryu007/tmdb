//
//  ViewController.swift
//  themoviedbapp
//
//  Created by BAGOES123 on 23/05/22.
//

import UIKit

class MovieViewController: UITableViewController {

    @IBOutlet var searchBar: UISearchBar!
    
    var currentPage = 1
    var presenter: MoviePresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.titleView = searchBar
        searchBar.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        presenter?.loadMovies(page: currentPage, keyword: nil)
    }
}

extension MovieViewController: MoviePresenterToViewProtocol {
    func updateMoviesData() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func showError() {
        let alert = UIAlertController(title: "Error", message: "Terjadi kesalahan", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
}

extension MovieViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if (searchText == ""){
            presenter?.loadMovies(page: currentPage, keyword: nil)
            return
        }
        presenter?.movies.removeAll()
        currentPage = 1
        presenter?.loadMovies(page: currentPage, keyword: searchBar.text)
    }
}

extension MovieViewController {
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter?.movies.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "movieCell") as? MovieTableViewCell
        
        if let movie = presenter?.movies[indexPath.row] {
            cell?.bindData(movie: movie)
        }
        
        return cell!
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let movie = presenter?.movies[indexPath.row] {
            navigationController?.pushViewController(MovieRouter.goToDetail(movies: movie), animated: true)
        }
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        
        if offsetY > contentHeight - scrollView.frame.size.height {
            currentPage = currentPage + 1
            presenter?.loadMovies(page: currentPage, keyword: searchBar.text)
        }
    }
    
}

